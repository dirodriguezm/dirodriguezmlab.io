#+TITLE: Blog Index

- *[[file:get-coverage-reports-from-container.org][Get Coverage Reports From a Flask Application Running Inside Docker Container]]*
  #+html: <p class='pubdate'>by Diego Rodriguez Mancini on Jul 17, 2022.</p>
- *[[file:dependency_injector_example.org][Applying Dependency Injection in a Flask Application]]*
  #+html: <p class='pubdate'>by Diego Rodriguez Mancini on Apr 09, 2022.</p>
- *[[file:mongo_orm_python.org][Creating a Simple Mongo ORM in Python]]*
  #+html: <p class='pubdate'>by Diego Rodriguez Mancini on Oct 17, 2021.</p>
- *[[file:nuxt-clean-architecture-part4.org][Implementing a Clean Architecture Modular Application in Nuxt/Vue Typescript Part 4: UI Components]]*
  #+html: <p class='pubdate'>by Diego Rodriguez Mancini on Feb 28, 2021.</p>
- *[[file:nuxt-clean-architecture-part3.org][Implementing a Clean Architecture Modular Application in Nuxt/Vue Typescript Part 3: Vuex Store]]*
  #+html: <p class='pubdate'>by Diego Rodriguez Mancini on Feb 26, 2021.</p>
- *[[file:nuxt-clean-architecture-part2.org][Implementing a Clean Architecture Modular Application in Nuxt/Vue Typescript Part 2: Services]]*
  #+html: <p class='pubdate'>by Diego Rodriguez Mancini on Feb 24, 2021.</p>
- *[[file:nuxt-clean-architecture.org][Implementing a Clean Architecture Modular Application in Nuxt/Vue Typescript Part 1: Domain Layer]]*
  #+html: <p class='pubdate'>by Diego Rodriguez Mancini on Feb 20, 2021.</p>
- *[[file:vuex-module-communication.org][Vuex Module Communication Using Nuxt with Nuxt Property Decorators]]*
  #+html: <p class='pubdate'>by Diego Rodriguez Mancini on Aug 01, 2020.</p>